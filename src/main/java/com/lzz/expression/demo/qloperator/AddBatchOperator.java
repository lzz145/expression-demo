package com.lzz.expression.demo.qloperator;

import com.ql.util.express.Operator;
import com.ql.util.express.OperatorOfNumber;

/**
 * 累加操作符
 * @author: liuzhengzhong
 * @create: 2020/11/2 4:45 PM
 **/
public class AddBatchOperator extends Operator {

        public AddBatchOperator(String aName) {
            this.name = aName;
        }

        @Override
        public Object executeInner(Object[] list) throws Exception {
            Object result = new Integer(0);
            for (int i = 0; i < list.length; i++) {
                result = OperatorOfNumber.add(result, list[i], true);
            }
            return result;
        }
}
