package com.lzz.expression.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExpressionDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExpressionDemoApplication.class, args);
    }

}
