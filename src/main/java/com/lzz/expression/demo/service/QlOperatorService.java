package com.lzz.expression.demo.service;

import com.lzz.expression.demo.entity.QlOperator;
import com.lzz.expression.demo.entity.QlRule;
import java.util.List;

/**
 * @author: liuzhengzhong
 * @create: 2020/10/27 3:48 PM
 **/
public interface QlOperatorService {


    QlOperator save(QlOperator qlOperator);

    List<QlOperator> findByRuleIds(List<Long> ids);
}
