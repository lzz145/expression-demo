package com.lzz.expression.demo.service;

import com.lzz.expression.demo.entity.QlMacro;
import com.lzz.expression.demo.entity.QlOperator;

/**
 * @author: liuzhengzhong
 * @create: 2020/11/3 9:47 AM
 **/
public interface QlMacroService {

    QlMacro save(QlMacro qlMacro);
}
