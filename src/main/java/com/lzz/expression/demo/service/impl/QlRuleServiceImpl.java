package com.lzz.expression.demo.service.impl;

import com.lzz.expression.demo.entity.QlRule;
import com.lzz.expression.demo.repository.QlRuleRepository;
import com.lzz.expression.demo.service.QlRuleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author: liuzhengzhong
 * @create: 2020/10/28 1:49 PM
 **/
@Service
@Slf4j
public class QlRuleServiceImpl implements QlRuleService {

    @Autowired
    private QlRuleRepository qlruleRepository;
    @Override
    public QlRule save(QlRule qlRule) {
        return qlruleRepository.save(qlRule);
    }

    @Override
    public QlRule findByCode(String code) {
        QlRule qlRule = qlruleRepository.findByCode(code);
        return qlRule;
    }
}
