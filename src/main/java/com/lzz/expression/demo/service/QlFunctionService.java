package com.lzz.expression.demo.service;

import com.lzz.expression.demo.entity.QlFunction;
import com.lzz.expression.demo.entity.QlOperator;
import java.util.List;

/**
 * @author: liuzhengzhong
 * @create: 2020/10/27 3:48 PM
 **/
public interface QlFunctionService {


    QlFunction save(QlFunction qlFunction);

    List<QlFunction> findByRuleIds(List<Long> functionidList);
}
