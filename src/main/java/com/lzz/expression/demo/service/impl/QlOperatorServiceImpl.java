package com.lzz.expression.demo.service.impl;

import com.lzz.expression.demo.entity.QlOperator;
import com.lzz.expression.demo.repository.QlOperatorRepository;
import com.lzz.expression.demo.service.QlOperatorService;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author: liuzhengzhong
 * @create: 2020/11/2 7:32 PM
 **/
@Slf4j
@Service
public class QlOperatorServiceImpl implements QlOperatorService {

    @Autowired
    private QlOperatorRepository qlOperatorRepository;

    @Override
    public QlOperator save(QlOperator qlOperator) {
        return qlOperatorRepository.save(qlOperator);
    }

    @Override
    public List<QlOperator> findByRuleIds(List<Long> ids) {
        qlOperatorRepository.findAllById(ids);
        return null;
    }
}
