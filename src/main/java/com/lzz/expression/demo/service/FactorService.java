package com.lzz.expression.demo.service;

import com.lzz.expression.demo.entity.Factor;
import java.util.List;

/**
 * @author: liuzhengzhong
 * @create: 2020/10/27 3:48 PM
 **/
public interface FactorService {

    Factor save(Factor factor);

    List<Factor> findAll();

    Factor update(Factor factor);

    List<Factor> saveList(List<Factor> list);

    List<Factor> findByRuleId(Long id);
}
