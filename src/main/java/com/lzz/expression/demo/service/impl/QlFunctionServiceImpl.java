package com.lzz.expression.demo.service.impl;

import com.lzz.expression.demo.entity.QlFunction;
import com.lzz.expression.demo.repository.QlFunctionRepository;
import com.lzz.expression.demo.service.QlFunctionService;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author: liuzhengzhong
 * @create: 2020/11/2 7:33 PM
 **/
@Service
@Slf4j
public class QlFunctionServiceImpl implements QlFunctionService {

    @Autowired
    private QlFunctionRepository qlFunctionRepository;

    @Override
    public QlFunction save(QlFunction qlFunction) {
        return qlFunctionRepository.save(qlFunction);
    }

    @Override
    public List<QlFunction> findByRuleIds(List<Long> functionidList) {
        List<QlFunction> allById = qlFunctionRepository.findAllById(functionidList);
        return allById;
    }
}
