package com.lzz.expression.demo.service.impl;

import com.lzz.expression.demo.entity.Factor;
import com.lzz.expression.demo.repository.FactorRepository;
import com.lzz.expression.demo.service.FactorService;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author: liuzhengzhong
 * @create: 2020/10/27 3:55 PM
 **/
@Slf4j
@Service
public class FactorServiceImpl implements FactorService {

    @Autowired
    private FactorRepository factorRepository;

    @Override
    public Factor save(Factor factor) {
        Factor save = factorRepository.save(factor);
        return save;
    }

    @Override
    public List<Factor> findAll() {
        List<Factor> all = factorRepository.findAll();
        return all;
    }

    @Override
    public Factor update(Factor factor) {
        if(factor.getId()==null){
            throw new RuntimeException("更新操作失败，没有主键");
        }
        Factor update = factorRepository.save(factor);
        return update;
    }

    @Override
    public List<Factor> saveList(List<Factor> list) {
        List<Factor> factors = factorRepository.saveAll(list);
        return factors;
    }

    @Override
    public List<Factor> findByRuleId(Long id) {
        List<Factor> list = factorRepository.findByRuleId(id);
        return list;
    }
}
