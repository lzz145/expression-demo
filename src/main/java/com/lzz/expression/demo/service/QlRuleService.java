package com.lzz.expression.demo.service;

import com.lzz.expression.demo.entity.QlRule;

/**
 * @author: liuzhengzhong
 * @create: 2020/10/27 3:48 PM
 **/
public interface QlRuleService {

    QlRule save(QlRule factor);

    QlRule findByCode(String code);
}
