package com.lzz.expression.demo.service.impl;

import com.lzz.expression.demo.entity.Rule;
import com.lzz.expression.demo.repository.RuleRepository;
import com.lzz.expression.demo.service.RuleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author: liuzhengzhong
 * @create: 2020/10/28 1:49 PM
 **/
@Service
@Slf4j
public class RuleServiceImpl implements RuleService {

    @Autowired
    private RuleRepository ruleRepository;
    @Override
    public Rule save(Rule rule) {
        return ruleRepository.save(rule);
    }

    @Override
    public Rule findByCode(String code) {
        Rule rule = ruleRepository.findByCode(code);
        return rule;
    }
}
