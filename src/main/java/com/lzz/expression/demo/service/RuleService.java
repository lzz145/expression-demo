package com.lzz.expression.demo.service;

import com.lzz.expression.demo.entity.Rule;

/**
 * @author: liuzhengzhong
 * @create: 2020/10/27 3:48 PM
 **/
public interface RuleService {

    Rule save(Rule factor);

    Rule findByCode(String code);
}
