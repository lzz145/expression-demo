package com.lzz.expression.demo.service.impl;

import com.lzz.expression.demo.entity.QlMacro;
import com.lzz.expression.demo.repository.QlMacroRepository;
import com.lzz.expression.demo.service.QlMacroService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author: liuzhengzhong
 * @create: 2020/11/3 9:47 AM
 **/
@Slf4j
@Service
public class QlMacroServiceImpl implements QlMacroService {

    @Autowired
    private QlMacroRepository qlMacroRepository;

    @Override
    public QlMacro save(QlMacro qlMacro) {
        return qlMacroRepository.save(qlMacro);
    }
}
