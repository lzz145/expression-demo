package com.lzz.expression.demo.dto;

import java.util.Map;
import lombok.Data;

/**
 * @author: liuzhengzhong
 * @create: 2020/10/27 5:30 PM
 **/
@Data
public class AviatorEngineData extends EngineData  {

}
