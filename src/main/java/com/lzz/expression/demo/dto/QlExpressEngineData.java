package com.lzz.expression.demo.dto;

import com.lzz.expression.demo.entity.QlFunction;
import com.lzz.expression.demo.entity.QlOperator;
import java.util.List;
import lombok.Data;

/**
 * @author: liuzhengzhong
 * @create: 2020/10/27 5:30 PM
 **/
@Data
public class QlExpressEngineData extends EngineData  {

    private List<QlOperator> qlOperators;

    private List<QlFunction> qlFunctions;

    private Boolean supportDynamicParams;
}
