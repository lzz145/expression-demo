package com.lzz.expression.demo.calculation;

import com.lzz.expression.demo.dto.EngineData;

/**
 * @author: liuzhengzhong
 * @create: 2020/10/27 4:34 PM
 **/
public interface Engine<T> {

   Object executeScript(T engineData);

}
