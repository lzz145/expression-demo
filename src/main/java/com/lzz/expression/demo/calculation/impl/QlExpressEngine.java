package com.lzz.expression.demo.calculation.impl;

import cn.hutool.core.util.ReflectUtil;
import cn.hutool.system.UserInfo;
import com.lzz.expression.demo.calculation.Engine;
import com.lzz.expression.demo.dto.QlExpressEngineData;
import com.lzz.expression.demo.entity.QlFunction;
import com.lzz.expression.demo.entity.QlOperator;
import com.lzz.expression.demo.qloperator.AddBatchOperator;
import com.ql.util.express.ExpressRunner;
import java.util.Collection;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

/**
 * @author: liuzhengzhong
 * @create: 2020/10/27 4:39 PM
 **/
@Component("qlExpressEngine")
public class QlExpressEngine implements Engine<QlExpressEngineData> {


    @Override
    public Object executeScript(QlExpressEngineData engineData) {
        try {
            ExpressRunner runner = new ExpressRunner();
            if(!CollectionUtils.isEmpty(engineData.getQlOperators())){
                for (QlOperator qlOperator : engineData.getQlOperators()) {
                    if(1==qlOperator.getOperatorType()){
                        runner.addOperatorWithAlias(qlOperator.getAliasName(), qlOperator.getOperator(), qlOperator.getErrorInfo());
                    }else{
                        runner.addOperator(qlOperator.getAliasName(), ReflectUtil.newInstance(qlOperator.getOperator()));
                    }
                }
            }
            if(!CollectionUtils.isEmpty(engineData.getQlFunctions())){
                for (QlFunction qlFunction : engineData.getQlFunctions()) {
                    if(1==qlFunction.getFunctionType()){
                       runner.addFunctionOfClassMethod(qlFunction.getScriptName(),qlFunction.getClassName(),qlFunction
                           .getFunctionName(),qlFunction.getParams().split("#"),qlFunction.getErrorInfo());
                    }else{
                        runner.addFunctionOfServiceMethod(qlFunction.getScriptName(),qlFunction.getClassName(),qlFunction
                            .getFunctionName(),qlFunction.getParams().split("#"),qlFunction.getErrorInfo());
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        AddBatchOperator addBatchOperator = ReflectUtil.newInstance(AddBatchOperator.class,"累加");
        return null;
    }
}
