package com.lzz.expression.demo.calculation.impl;

import cn.hutool.crypto.SecureUtil;
import com.googlecode.aviator.AviatorEvaluator;
import com.googlecode.aviator.Expression;
import com.lzz.expression.demo.calculation.Engine;
import com.lzz.expression.demo.dto.AviatorEngineData;
import org.springframework.stereotype.Component;

/**
 * @author: liuzhengzhong
 * @create: 2020/10/27 4:39 PM
 **/
@Component("aviatorEngine")
public class AviatorEngine implements Engine<AviatorEngineData> {

    @Override
    public Object executeScript(AviatorEngineData msg) {
        String key = SecureUtil.md5(msg.getScript());
         Expression exp = AviatorEvaluator.getInstance().compile(key,msg.getScript(),true);
        Object result = exp.execute(msg.getParams());
        if(result==null){
            throw new RuntimeException("executeScript 返回结果为空");
        }
        return result;
    }
}
