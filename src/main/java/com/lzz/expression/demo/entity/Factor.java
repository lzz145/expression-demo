package com.lzz.expression.demo.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 因子
 * @author: liuzhengzhong
 * @create: 2020/10/27 10:39 AM
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "factor",
    uniqueConstraints=@UniqueConstraint(name = "uk_rule_id_filed_name",columnNames={"ruleId","filedName"}))
public class Factor implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @Column(columnDefinition="bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '所属规则id'")
    private Long ruleId;

    @Column(columnDefinition="varchar(20) NOT NULL COMMENT '因子名称'")
    private String name;

    @Column(name = "`desc`",columnDefinition="varchar(128) COMMENT '因子描述'")
    private String desc;

    @Column(columnDefinition="varchar(30) NOT NULL COMMENT '参数名称'")
    private String filedName;

    @Column(columnDefinition="TINYINT(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '因子状态（1：有效  0：无效）'")
    private Integer status;

    @Column(columnDefinition="TINYINT(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '因子数据类型'")
    private Integer dateType;

    @Column(name = "`value`",columnDefinition="varchar(32) NOT NULL DEFAULT '' COMMENT '因子值'")
    private String value;



}
