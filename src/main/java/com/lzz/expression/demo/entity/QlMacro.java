package com.lzz.expression.demo.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author: liuzhengzhong
 * @create: 2020/10/28 1:35 PM
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "ql_macro")
public class QlMacro implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @Column(columnDefinition="varchar(20) NOT NULL COMMENT '名称'")
    private String name;

    @Column(columnDefinition="varchar(20) NOT NULL COMMENT '宏'")
    private String macro;


}
