package com.lzz.expression.demo.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author: liuzhengzhong
 * @create: 2020/10/28 1:35 PM
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "ql_operator")
public class QlOperator implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @Column(columnDefinition="varchar(20) NOT NULL COMMENT '别名'")
    private String aliasName;

    @Column(columnDefinition="varchar(20) NOT NULL COMMENT '操作符'")
    private String operator;

    @Column(columnDefinition="TINYINT(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '操作符类型(1:原生 2:自定义)'")
    private Integer operatorType;

    @Column(columnDefinition="varchar(1024) COMMENT '错误信息'")
    private String errorInfo;

}
