package com.lzz.expression.demo.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author: liuzhengzhong
 * @create: 2020/10/28 1:35 PM
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "rule")
public class Rule implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @Column(columnDefinition="varchar(32) NOT NULL COMMENT '规则编码'")
    private String code;

    @Column(columnDefinition="varchar(20) NOT NULL COMMENT '规则名称'")
    private String name;

    @Column(name = "`desc`",columnDefinition="varchar(128) COMMENT '规则描述'")
    private String desc;

    @Column(columnDefinition="TINYINT(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '规则状态（1：有效  0：无效）'")
    private Integer status;

    @Column(name = "`level`",columnDefinition=" SMALLINT(15) UNSIGNED NOT NULL DEFAULT 1 COMMENT '规则优先级（值越大优先级越高）'")
    private Integer level;

    @Column(columnDefinition="varchar(1024) COMMENT '执行脚本'")
    private String script;

    @Column(columnDefinition="TINYINT(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '返回类型(1:Boolean 2:json 3:string 4:null)'")
    private Integer resultType;

}
