package com.lzz.expression.demo.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author: liuzhengzhong
 * @create: 2020/10/28 1:35 PM
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "ql_function")
public class QlFunction implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @Column(columnDefinition="varchar(20) NOT NULL COMMENT '脚本函数名'")
    private String scriptName;

    @Column(columnDefinition="varchar(50) NOT NULL COMMENT '引用类名'")
    private String className;

    @Column(columnDefinition="varchar(20) NOT NULL COMMENT '引用函数名'")
    private String functionName;

    @Column(columnDefinition="varchar(1024) NOT NULL COMMENT '参数class名称列表，#号隔开'")
    private String params;

    @Column(columnDefinition="TINYINT(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '函数类型(1:java类 2:自定义对象)'")
    private Integer functionType;

    @Column(columnDefinition="varchar(1024) COMMENT '错误信息'")
    private String errorInfo;

}
