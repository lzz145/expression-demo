package com.lzz.expression.demo.enums;

import java.math.BigDecimal;
import java.math.BigInteger;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum DataTypeEnum {
    /**
     * 字符串
     */
    STRING(1, String.class),
    LONG(2, Long.class),
    BIGINTEGER(3, BigInteger.class),
    DOUBLE(4, Double.class),
    BIGDECIMAL(5, BigDecimal.class),
    BOOLEAN(6,Boolean.class)
    ;

    @Getter
    public int code;

    @Getter
    public Class type;

    public static Class getTypeByCode(Integer code) {
        DataTypeEnum[] dataTypeEnums = values();
        for (DataTypeEnum dataTypeEnum : dataTypeEnums) {
            if (dataTypeEnum.getCode()==(code)) {
                return dataTypeEnum.getType();
            }
        }
        return null;
    }

}
