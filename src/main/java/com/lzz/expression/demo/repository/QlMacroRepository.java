package com.lzz.expression.demo.repository;

import com.lzz.expression.demo.entity.QlMacro;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author: liuzhengzhong
 * @create: 2020/10/27 3:43 PM
 **/
public interface QlMacroRepository extends JpaRepository<QlMacro, Long> {

}
