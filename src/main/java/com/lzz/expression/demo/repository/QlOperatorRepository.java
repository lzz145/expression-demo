package com.lzz.expression.demo.repository;

import com.lzz.expression.demo.entity.QlOperator;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author: liuzhengzhong
 * @create: 2020/10/27 3:43 PM
 **/
public interface QlOperatorRepository extends JpaRepository<QlOperator, Long> {

}
