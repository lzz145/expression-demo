package com.lzz.expression.demo.repository;

import com.lzz.expression.demo.entity.QlFunction;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author: liuzhengzhong
 * @create: 2020/10/27 3:43 PM
 **/
public interface QlFunctionRepository extends JpaRepository<QlFunction, Long> {

}
