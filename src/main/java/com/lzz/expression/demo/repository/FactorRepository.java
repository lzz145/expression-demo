package com.lzz.expression.demo.repository;

import com.lzz.expression.demo.entity.Factor;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author: liuzhengzhong
 * @create: 2020/10/27 3:43 PM
 **/
public interface FactorRepository extends JpaRepository<Factor, Long> {

    List<Factor> findByRuleId(Long id);
}
