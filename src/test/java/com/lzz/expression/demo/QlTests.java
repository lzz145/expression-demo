package com.lzz.expression.demo;

import ch.qos.logback.core.db.dialect.SybaseSqlAnywhereDialect;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.ListUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.ReflectUtil;
import com.lzz.expression.demo.calculation.Engine;
import com.lzz.expression.demo.dto.EngineData;
import com.lzz.expression.demo.dto.QlExpressEngineData;
import com.lzz.expression.demo.entity.Factor;
import com.lzz.expression.demo.entity.QlFunction;
import com.lzz.expression.demo.entity.QlMacro;
import com.lzz.expression.demo.entity.QlOperator;
import com.lzz.expression.demo.entity.QlRule;
import com.lzz.expression.demo.entity.Rule;
import com.lzz.expression.demo.enums.DataTypeEnum;
import com.lzz.expression.demo.qloperator.AddBatchOperator;
import com.lzz.expression.demo.service.FactorService;
import com.lzz.expression.demo.service.QlFunctionService;
import com.lzz.expression.demo.service.QlMacroService;
import com.lzz.expression.demo.service.QlOperatorService;
import com.lzz.expression.demo.service.QlRuleService;
import com.lzz.expression.demo.service.RuleService;
import com.ql.util.express.DefaultContext;
import com.ql.util.express.ExpressRunner;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
class QlTests {

    @Autowired
    QlRuleService qlRuleService;
    @Autowired
    QlOperatorService qlOperatorService;
    @Autowired
    QlFunctionService qlFunctionService;
    @Autowired
    QlMacroService qlMacroService;
    @Autowired
    Engine qlExpressEngine;
    @Test
    void initRule() {
        QlOperator qlOperator=new QlOperator();
        qlOperator.setAliasName("而且");
        qlOperator.setOperator("and");
        qlOperator.setOperatorType(1);
        qlOperator.setErrorInfo(null);
        QlOperator operator = qlOperatorService.save(qlOperator);

        QlFunction qlFunction=new QlFunction();
        qlFunction.setScriptName("userTagJudge");
        qlFunction.setClassName(CheckUtil.class.getName());
        qlFunction.setFunctionName("userTagJudge");
        qlFunction.setParams(CollUtil.join(Arrays.asList(UserInfo.class.getName(),int.class.getName()),"#"));
        qlFunction.setFunctionType(2);
        qlFunction.setErrorInfo("你不是三星卖家");
        QlFunction saveqlFunction = qlFunctionService.save(qlFunction);

        QlFunction qlFunction2=new QlFunction();
        qlFunction2.setScriptName("hasOrderGoods");
        qlFunction2.setClassName(CheckUtil.class.getName());
        qlFunction2.setFunctionName("hasOrderGoods");
        qlFunction2.setParams(CollUtil.join(Arrays.asList(UserInfo.class.getName(),long.class.getName()),"#"));
        qlFunction2.setFunctionType(2);
        qlFunction2.setErrorInfo("你没有开通淘宝店铺");
        QlFunction saveqlFunction2 = qlFunctionService.save(qlFunction2);

        QlMacro qlMacro=new QlMacro();
        qlMacro.setName("三星卖家");
        qlMacro.setMacro("userTagJudge(userInfo,3)");
        QlMacro saveqlMacro = qlMacroService.save(qlMacro);

        QlMacro qlMacro2=new QlMacro();
        qlMacro2.setName("已经开店");
        qlMacro2.setMacro("hasOrderGoods(userInfo,100)");
        QlMacro saveqlMacro2 = qlMacroService.save(qlMacro2);

        String script = "三星卖家   而且   已经开店";
        QlRule rule=new QlRule();
        rule.setCode(IdUtil.simpleUUID());
        rule.setName("订阅权限");
        rule.setDesc("判断是否能订阅");
        rule.setStatus(1);
        rule.setLevel(1);
        rule.setScript(script);
        rule.setResultType(1);
        rule.setOperator(CollUtil.join(Arrays.asList(operator.getId()),"#"));
        rule.setMethodFunction(CollUtil.join(Arrays.asList(saveqlFunction.getId(),saveqlFunction2.getId()),"#"));
        rule.setMacro(CollUtil.join(Arrays.asList(saveqlMacro.getId(),saveqlMacro2.getId()),"#"));
        QlRule saveRule = qlRuleService.save(rule);

    }

    @Test
    void test() throws Exception {
        QlRule byCode = qlRuleService.findByCode("97247264f5c045f6862d9798cf5f5c24");
        List<Long> operatoridList = Arrays.asList(byCode.getOperator().split("#")).stream().map(s -> {
            return Long.valueOf(s);
        }).collect(Collectors.toList());
        List<QlOperator> qlOperators = qlOperatorService.findByRuleIds(operatoridList);

        List<Long> functionidList = Arrays.asList(byCode.getMethodFunction().split("#")).stream().map(s -> {
            return Long.valueOf(s);
        }).collect(Collectors.toList());
        List<QlFunction> qlFunctions = qlFunctionService.findByRuleIds(functionidList);

        QlExpressEngineData engineData=new QlExpressEngineData();
        engineData.setQlOperators(qlOperators);
        engineData.setQlFunctions(qlFunctions);
        engineData.setSupportDynamicParams(byCode.getSupportDynamicParams());
        Map<String,Object> params=new HashMap<>();
        params.put("userInfo", new UserInfo(100,"xuannan",7));
        engineData.setParams(params);
        engineData.setScript(byCode.getScript());
        Boolean result = (Boolean)qlExpressEngine.executeScript(engineData);
        System.out.println(result);

    }
    @Test
    public void testQLExpressAlias() throws Exception {
        ExpressRunner runner = new ExpressRunner();
        runner.addOperatorWithAlias("而且", "and", null);

        DefaultContext<String, Object> context = new DefaultContext<String, Object>();
        context.put("samples", 1);
        //String exp = "如果 (如果 1==2 则 false 否则 true) 则 {2+2;} 否则 {20 + 20;}";
        String exp = "如果 (samples==1) 则{ 222+111; }否则 {20 + 20;}";
        Object r = runner.execute(exp, context, null, false, false, null);
        System.out.println(r);
    }

    public static void main(String[] args) {
        System.out.println(String.class.getName());
    }


}
