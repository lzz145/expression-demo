package com.lzz.expression.demo;

/**
 * @author: liuzhengzhong
 * @create: 2020/11/2 7:44 PM
 **/
public class UserInfo {
    long id;
    long tag;
    String name;

    public UserInfo(long aId,String aName, long aUserTag) {
        this.id = aId;
        this.tag = aUserTag;
        this.name = aName;
    }
    public String getName(){
        return name;
    }
    public long getUserId() {
        return id;
    }

    public long getUserTag() {
        return tag;
    }
}
