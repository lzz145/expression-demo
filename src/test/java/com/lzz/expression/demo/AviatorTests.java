package com.lzz.expression.demo;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import com.lzz.expression.demo.calculation.Engine;
import com.lzz.expression.demo.dto.EngineData;
import com.lzz.expression.demo.entity.Factor;
import com.lzz.expression.demo.entity.Rule;
import com.lzz.expression.demo.enums.DataTypeEnum;
import com.lzz.expression.demo.service.FactorService;
import com.lzz.expression.demo.service.RuleService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
class AviatorTests {

    @Autowired
    RuleService ruleService;
    @Autowired
    FactorService factorService;
    @Autowired
    Engine aviatorEngine;
    @Test
    void initRule() {
        String script="if !newUser {\n"
            + "            if (weights>3 && weights<9){\n"
            + "               return true;\n"
            + "            }\n"
            + "        }\n"
            + "        return false;";


        Rule rule=new Rule();
        rule.setCode(IdUtil.simpleUUID());
        rule.setName("权限校验");
        rule.setDesc("权限校验");
        rule.setStatus(1);
        rule.setLevel(1);
        rule.setScript(script);
        rule.setResultType(1);
        Rule saveRule = ruleService.save(rule);

        List<Factor> list=new ArrayList<>();
        Factor factor=new Factor();
        factor.setName("权重");
        factor.setDesc("权重");
        factor.setRuleId(saveRule.getId());
        factor.setStatus(1);
        factor.setFiledName("weights");
        factor.setDateType(2);
        factor.setValue("8");
        list.add(factor);

        Factor factor2=new Factor();
        factor2.setName("是否新用户");
        factor2.setDesc("是否新用户");
        factor2.setRuleId(saveRule.getId());
        factor2.setStatus(1);
        factor2.setFiledName("newUser");
        factor2.setDateType(6);
        factor2.setValue("false");
        list.add(factor2);

        factorService.saveList(list);

    }

    @Test
    void test(){
        Rule byCode = ruleService.findByCode("97247264f5c045f6862d9798cf5f5c24");
        List<Factor> all = factorService.findByRuleId(byCode.getId());
        EngineData engineData=new EngineData();
        Map<String,Object> params=new HashMap<>();
        for (Factor factor : all) {
            Class typeClass = DataTypeEnum.getTypeByCode(factor.getDateType());
            if(typeClass==null){
                params.put(factor.getFiledName(), factor.getValue());
            }
            params.put(factor.getFiledName(), Convert.convert(typeClass,factor.getValue()));

        }
        engineData.setParams(params);
        engineData.setScript(byCode.getScript());
        //Boolean result = (Boolean)engine.executeScript(engineData);
        String result = (String)aviatorEngine.executeScript(engineData);
        System.out.println(result);

    }

    public static void main(String[] args) {
        boolean equal = ObjectUtil.equal(null, null);
        System.out.println(equal);
    }


}
